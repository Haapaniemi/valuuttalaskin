package com.example.clevername.valuuttalaskin;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Spinner currencySpinner = (Spinner) findViewById(R.id.spinner1);
        ArrayAdapter<String> myAdapter = new ArrayAdapter<String>(MainActivity.this,
                android.R.layout.simple_list_item_1,getResources().getStringArray(R.array.currencies));
        myAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        currencySpinner.setAdapter(myAdapter);
    }

    public void convert(View view) {

        EditText currency = findViewById(R.id.currencyInput1);
        Spinner mySpinner1 = findViewById(R.id.spinner1);
        String spinner1Currency = mySpinner1.getSelectedItem().toString();
        Spinner mySpinner2 = findViewById(R.id.spinner2);
        String spinner2Currency = mySpinner2.getSelectedItem().toString();
        Double euroToDollar = 1.13;
        Double dollarToEuro = 0.88;



        if(spinner1Currency.equals("EUR")) {
            Double euroAmount = Double.parseDouble(currency.getText().toString());
            if(spinner2Currency.equals("USD")){
                Double dollarValue = euroAmount * euroToDollar;
                ((TextView)findViewById(R.id.convertResult)).setText(euroAmount + " euros = " + dollarValue + " dollars");
            }else if(spinner2Currency.equals("EUR")){
                Double euroValue = euroAmount;
                ((TextView)findViewById(R.id.convertResult)).setText(euroAmount + " euros = " + euroValue + " euros");
            }

        }else if(spinner1Currency.equals("USD")) {
            Double dollarAmount = Double.parseDouble(currency.getText().toString());
            if(spinner2Currency.equals("EUR")) {
                Double euroValue = dollarAmount * dollarToEuro;
                ((TextView)findViewById(R.id.convertResult)).setText(dollarAmount + " dollars = " + euroValue + " euros");
            }else if(spinner2Currency.equals("USD")) {
                Double dollarValue = dollarAmount;
                ((TextView)findViewById(R.id.convertResult)).setText(dollarAmount + " dollars = " + dollarValue + " dollars");
            }

        }
    }
}
